# Views Autocomplete Search 
uses the [Views autocomplete API](https://www.drupal.org/project/views_autocomplete_api) module (it is a dependency with Views module of course) to transform the search input of the default search block into an autocomplete textfield.

The module simply packages a tiny form alteration and a view that you can customize. The default configuration search on the title only (contains the input or contains all words of the input) but you can change it by simply editing the packaged view.

Note : this module demonstrates how you can enhance very easily any textfield of your website with [Views autocomplete API](https://www.drupal.org/project/views_autocomplete_api) to turn it into an autocomplete textfield.

**Supporting organizations:**

[emerya](https://www.drupal.org/emerya) developpement


[DevBranch](https://www.drupal.org/devbranch) Development and maintenance
